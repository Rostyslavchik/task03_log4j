package smslogging;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSSender {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID
            = "AC3c40ab25c0359ad7328cfd2cf492b508";
    public static final String AUTH_TOKEN
            = "326fd81c500e7bb790513c687e740b67";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380978391160"),
                new PhoneNumber("+19166340971"), str).create();
    }
}
