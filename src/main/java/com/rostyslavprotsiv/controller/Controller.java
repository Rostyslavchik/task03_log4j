package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.PhoneAction;
import com.rostyslavprotsiv.model.entity.Phone;
import com.rostyslavprotsiv.model.exception.PhoneLogicalException;
import com.rostyslavprotsiv.model.generator.PhoneGenerator;
import com.rostyslavprotsiv.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    private PhoneAction action = new PhoneAction();
    private Menu menu = new Menu();
    private PhoneGenerator generator = new PhoneGenerator();

    public void execute() throws UnsupportedOperationException,
            PhoneLogicalException {
        menu.welcome();
        int optionNumber = menu.inputOption();
        if (optionNumber == 1) {
            executeFirstMethod();
        } else if (optionNumber == 2) {
            executeSecondMethod();
        } else {
            logger.trace(" trace UnsupportedOperationException");
            logger.debug(" debug UnsupportedOperationException");
            logger.info(" info UnsupportedOperationException");
            logger.warn(" warn UnsupportedOperationException");
            logger.error(" error UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            logger.trace(" trace UnsupportedOperationException");
            logger.debug(" debug UnsupportedOperationException");
            logger.info(" info UnsupportedOperationException");
            logger.warn(" warn UnsupportedOperationException");
            logger.error(" error UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            logger.trace(" trace UnsupportedOperationException");
            logger.debug(" debug UnsupportedOperationException");
            logger.info(" info UnsupportedOperationException");
            logger.warn(" warn UnsupportedOperationException");
            logger.error(" error UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            logger.trace(" trace UnsupportedOperationException");
            logger.debug(" debug UnsupportedOperationException");
            logger.info(" info UnsupportedOperationException");
            logger.warn(" warn UnsupportedOperationException");
            logger.error(" error UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            logger.fatal(" fatal UnsupportedOperationException");
            throw new UnsupportedOperationException();
        }
    }

    private void executeFirstMethod() {
        List<Phone> phones = generator.generateRandom();
        double totalPrice = action.getTotalPrice(phones);
        menu.outFirstCase();
        menu.outListOfPhones(phones.toString());
        menu.outTotalPrice(totalPrice);
    }

    private void executeSecondMethod() throws PhoneLogicalException {
        double price;
        String company;
        menu.outSecondCase();
        price = menu.inputPrice();
        company = menu.inputCompany();
        menu.outPhone(new Phone(price, company).toString());
    }
}
