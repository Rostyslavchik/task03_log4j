/**
 * This module is a representation of {@code View} abstraction in MVC.
 *
 * @author Rostyslav Protsiv
 * @since 16.08.2021
 * @version 1.0.0
 */
package com.rostyslavprotsiv.view;
