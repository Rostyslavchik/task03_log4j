package com.rostyslavprotsiv.view;

import java.util.Scanner;

public final class Menu {
    public static final Scanner SCAN = new Scanner(System.in);
    private static final String PATH = "com.rostyslavprotsiv.";

    public void welcome() {
        System.out.println("Hi, welcome to my program");
        System.out.println("Please, choose what would you like to do: ");
        System.out.println("    1. Output total price of "
                + "randomly generated objects");
        System.out.println("    2. Output Phone object created by user");
    }

    public int inputOption() {
        return SCAN.nextInt();
    }

    public void outFirstCase() {
        System.out.println("You chose to get randomly generated objects "
                 + "and total price for them");
    }

    public void outSecondCase() {
        System.out.println("You chose to get Phone object created by you");
    }

    public void outPhone(String obj) {
        System.out.println(obj);
    }

    public void outListOfPhones(String phones) {
        System.out.println(phones.replaceAll(PATH, "\n"));
    }

    public void outTotalPrice(double price) {
        System.out.println("The total price for these objects is : " + price);
    }

    public double inputPrice() {
        System.out.println("Please, input the Phone price");
        return SCAN.nextDouble();
    }

    public String inputCompany() {
        SCAN.nextLine(); // eats enter key
        System.out.println("Please, input the Phone company");
        return SCAN.nextLine();
    }
}
