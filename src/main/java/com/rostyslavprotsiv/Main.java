package com.rostyslavprotsiv;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.model.exception.PhoneLogicalException;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        try {
            controller.execute();
        } catch (PhoneLogicalException e) {
            e.printStackTrace();
        }
    }
}
