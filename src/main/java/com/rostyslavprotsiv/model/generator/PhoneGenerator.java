package com.rostyslavprotsiv.model.generator;

import com.rostyslavprotsiv.model.entity.Phone;
import com.rostyslavprotsiv.model.exception.PhoneLogicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class PhoneGenerator {
    private static Logger logger = LogManager.getLogger(PhoneGenerator.class);
    public static final Random RANDOM = new Random();
    private static final double MAX_PRICE = 200;
//    private static final double MIN_PRICE = 2;
    private static final double MIN_PRICE = -200;
    private static final int MIN_SYMBOL_CODE = 97;
    private static final int MAX_SYMBOL_CODE = 122;
    private static final int MAX_WORD_LENGTH = 6;
    private static final int MAX_PHONES = 10;
    private static final int MIN_PHONES = 1;

    public List<Phone> generateRandom() {
        List<Phone> phones = new LinkedList();
        int phonesAmount = RANDOM.nextInt(MAX_PHONES
                - MIN_PHONES) + MIN_PHONES;
        for (int i = 0; i < phonesAmount; i++) {
            try {
                phones.add(new Phone(generatePrice(), generateWord()));
            } catch (PhoneLogicalException e) {
                logger.trace(e.getCause()+ " trace " + e.getMessage());
                logger.debug(e.getCause()+ " debug " + e.getMessage());
                logger.info(e.getCause()+ " info " + e.getMessage());
                logger.warn(e.getCause()+ " warn " + e.getMessage());
                logger.error(e.getCause()+ " error " + e.getMessage());
                logger.fatal(e.getCause()+ " fatal " + e.getMessage());
//                e.printStackTrace();
            }
        }
        return phones;
    }

    private String generateWord() {
        int wordLength = RANDOM.nextInt(MAX_WORD_LENGTH);
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < wordLength; i++) {
            word.append(generateSymbol());
        }
        return word.toString();
    }

    private char generateSymbol() {
        return (char) (RANDOM.nextInt(MAX_SYMBOL_CODE
                - MIN_SYMBOL_CODE) + MIN_SYMBOL_CODE);
    }

    private double generatePrice() {
        return (RANDOM.nextDouble() * (MAX_PRICE - MIN_PRICE) + MIN_PRICE);
    }
}
