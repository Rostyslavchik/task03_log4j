package com.rostyslavprotsiv.model.exception;

public class PhoneLogicalException extends Exception {
    public PhoneLogicalException() {}

    public PhoneLogicalException(String s) {
        super(s);
    }

    public PhoneLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PhoneLogicalException(Throwable throwable) {
        super(throwable);
    }
}
