package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Phone;

import java.util.List;

public class PhoneAction {
    /**
     * This method was created to sum up the price of all objects
     * @param phones The list of Phone objects
     * @return The total price
     *
     * @see List
     */
    public double getTotalPrice(List<Phone> phones) {
        double price = 0;
        for (Phone phone: phones) {
            price += phone.getPrice();
        }
        return price;
    }
}
