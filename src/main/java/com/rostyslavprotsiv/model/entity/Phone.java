package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exception.PhoneLogicalException;

import java.util.Objects;

public class Phone {
    private double price;
    private String company;

    public Phone() {
    }

    public Phone(double price, String company) throws PhoneLogicalException {
        if (checkPrice(price)) {
            this.price = price;
            this.company = company;
        } else {
            throw new PhoneLogicalException("Price is not suitable");
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) throws PhoneLogicalException {
        if (checkPrice(price)) {
            this.price = price;
        } else {
            throw new PhoneLogicalException("Price is not suitable");
        }
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    private boolean checkPrice(double price) {
        return price > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Double.compare(phone.price, price) == 0
                && Objects.equals(company, phone.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, company);
    }

    @Override
    public String toString() {
        return "Phone{"
                + "price=" + price
                + ", company='" + company + '\'' + '}';
    }
}
